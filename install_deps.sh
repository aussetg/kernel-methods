#!/bin/bash
julia -e 'Pkg.update(); Pkg.add("Iterators"); Pkg.add("MathProgBase"); Pkg.add("Optim"); Pkg.add("Mosek"); Pkg.add("Distances"); Pkg.add("ProgressMeter");'
