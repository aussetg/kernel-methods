#!/usr/bin/julia
push!(LOAD_PATH,"src/")
using Data, Kernels, SVM, Utils, ImageUtils

print("Importing data...")
Xtr = getData("data/Xtr.csv")
Ytr = getLabels("data/Ytr.csv")
Xtr = Xtr' # Julia prefers column major ordering. All functions will use it
print_with_color(:green, "✔\n")

print("Building features and dataset...")
augData = Matrix{Float64}((324,10000))
lbl = Vector{Int}(10000)

Threads.@threads for i =1:5000
  im = grayscale(toImage(Xtr[:,i], 32, 32))
  augData[:,i] = hog(im, (8,8), (2,2))
  lbl[i] = Ytr[i]
  im = mirror(im)
  augData[:,5000+i] = hog(im, (8,8), (2,2))
  lbl[5000+i] = Ytr[i]
end
x = VanillaDataset(augData, lbl)
print_with_color(:green, "✔\n")

print("Cleaning up data to give the GC some space...")
Xtr = 0
Ytr = 0
augData = 0
print_with_color(:green, "✔\n")

print("Using SMO (C = 1.7, 500 max iterations) with Gaussian kernel (γ = 0.5).")
g = GaussianKernel(0.5)
s = SMO(g, 1.7, 500)
print_with_color(:green, "✔\n")

print("Creating a OneVsRest classifier.")
clf = OneVsRest(s)
print_with_color(:green, "✔\n")
println("Training classifiers with MultiThreading (no progress bar available).")
SVM.fit(clf, x; progress = false, threading = false, precompute = true)

print("Importing test data...")
Xte = getData("data/Xte.csv")
Xte = Xte'
print_with_color(:green, "✔\n")

print("Building features and dataset...")
p, nt = size(Xte)
augDataTest = Matrix{Float64}((324,nt))
Threads.@threads for i = 1:nt
  im = grayscale(toImage(Xte[:,i], 32, 32))
  augDataTest[:,i] = hog(im)
end
xt = VanillaDataset(augDataTest)
print_with_color(:green, "✔\n")

print("Predicting our test data...")
preds = predict(clf, xt)
print_with_color(:green, "✔\n")
print("Writing predictions to file...")
writePredictions(preds, "data/Yte.csv")
print_with_color(:green, "✔\n")
