
\documentclass[twocolumn,10pt,a4paper]{article}
\usepackage[margin=0.80in,top=0.90in,bottom=0.80in]{geometry}
\usepackage{fontspec}
\usepackage{lmodern}
\usepackage{amsmath,amsthm,mathtools}
\usepackage{amsfonts,amssymb}
\usepackage{graphicx,overpic}
\usepackage{hyperref}
\usepackage{polyglossia,csquotes}
\setdefaultlanguage{english}
\usepackage{microtype}
\usepackage{dsfont}
\usepackage{booktabs}
\usepackage{float}
\usepackage{subfig}
\usepackage{xcolor}           
\usepackage{amsthm}                
\usepackage{enumerate}
\usepackage{fixltx2e}
\definecolor{darkgreen}{rgb}{0.0,0.55,0.0}
\usepackage[style=authoryear,hyperref,backref,natbib,
            backend=biber]{biblatex}
\bibliography{Biblio}
\usepackage{multicol}
\usepackage{float}
\usepackage{framed}
\usepackage{enumitem}
\usepackage{siunitx}

\usepackage[bottom]{footmisc}

\DeclareCiteCommand{\footpartcite}[\mkbibfootnote]
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   %\mkbibbrackets{\usebibmacro{cite}}%
   %\setunit{\addnbspace}
   \printnames{labelname}%
   \setunit{\labelnamepunct}
   \printfield[citetitle]{title}%
   \newunit
   \printfield{year}}
  {\addsemicolon\space}
  {\usebibmacro{postnote}}
  
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black},
    linktoc=page
}

\graphicspath{{fig/}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{remark}[theorem]{Remark}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}

\newcommand{\sgn}{\operatorname{sgn}}
\newcommand{\conv}{\operatorname{conv}}
\newcommand{\vect}{\operatorname{vect}}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand*\ri{\mathop{}\!\mathrm{ri}}
\newcommand*\aff{\mathop{}\!\mathrm{aff}}
\newcommand*\dom{\mathop{}\!\mathrm{dom}}
\newcommand*\epi{\mathop{}\!\mathrm{epi}}
\newcommand*\diag{\mathop{}\!\mathrm{diag}}
\newcommand*\cov{\mathop{}\!\mathrm{cov}}
\newcommand*\var{\mathop{}\!\mathrm{var}}
\newcommand*\corr{\mathop{}\!\mathrm{corr}}
\newcommand*\tr{\mathop{}\!\mathrm{tr}}

\usepackage{lastpage,fancyhdr}
\pagestyle{fancy}
    \renewcommand{\headrulewidth}{0.2pt}
    \renewcommand{\footrulewidth}{0.2pt}
    \lhead{\emph{Final Report -- Kaggle Data Challenge CIFAR10 - Kernel Methods 2017}}
    \rhead{\emph{\today}}
    \lfoot{Master MASH -- Université Paris Dauphine}
    \cfoot{$\thepage/\pageref{LastPage}$}
    \rfoot{\href{https://ausset.me}{G.Ausset}}


\providecommand*{\hr}[1][class-arg]{%
    \hspace*{\fill}\hrulefill\hspace*{\fill}
    \vskip 0.65\baselineskip
}

\newcommand{\range}{\mathrm{range}}
\newcommand{\eqdef}{\mathrel{\stackrel{\smash{\scriptscriptstyle\mathrm{def}}}{=}}}


\begin{document}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}

\normalsize
% ------------------------------------------------------------------------------
\title{\vspace*{-80pt}\textbf{Challenge Kaggle} \\\large{CIFAR-10 Image Classification}\\
\begin{footnotesize}
    \begin{center}
       \href{https://ausset.me}{Guillaume~Ausset} \\
        Paris Dauphine -- Master MASH \\
       \texttt{{guillaume}{@}{ausset}{.}{me}}
    \end{center}
\end{footnotesize}
\vspace{-1.5cm}}

\date{}
\maketitle

\setcounter{footnote}{-1}  %% XXX

% ------------------------------------------------------------------------------
% A small abstract of what is done in the paper
\begin{small}
\begin{abstract}
\begin{small}
This report will present the different methods tested as well as the methodology used for the project image classification for the validation of the course on Kernel Methods by J.P Vert and J. Mairal at the ENS Cachan. Everything has been implemented in \texttt{Julia} to make sure the performance of the language wasn't a bottleneck and to give me the ability to write easy to read code instead of having to vectorize everything. The code is organized in different modules that can act more or less independently and I have tried to comment it.
All the code is available here: \url{https://gitlab.com/aussetg/kernel-methods} and a \texttt{Docker} container is available at \url{https://hub.docker.com/r/aussetg/julia-mosek/} with all the dependencies already installed.
It is possible to download everything including the data with \texttt{GIT LFS} and to run my code using one of the various \texttt{start*.sh} scripts using either \texttt{Docker} or just \texttt{Julia}.
\end{small}
\end{abstract}
\end{small}


% ------------------------------------------------------------------------------
\hr{}  % Horizontal line, like <hr> in HTML
% XXX remove if it is ugly or take too much space


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \newpage
\normalsize % XXX

% ------------------------------------------------------------------------------
% ------------------------------------------------------------------------------
\section{Introduction}
\label{sec:intro}
% FIXED

This challenge consisted of a variant of the \texttt{CIFAR10}	 classification challenge where the goal is to correctly predict the class (\texttt{Int} from $0$ to $9$) of a natural image.
While the original \texttt{CIFAR10} dataset consists of $60000$ $32$x$32$ colour natural images, our dataset consisted of $5000$ $32$x$32$ colour images preprocessed by whitening. 
The goal was to predict the class of $2000$ unknown images. The minimum benchmark was set at $30$\% accuracy, which is roughly the score you can reach by applying an SVM on the raw features.

Several methods and directions have been explored and implemented which I will present in this report. The final model used is the following one:

\begin{center}
    \begin{footnotesize}
        \textbf{Kernel:} Radial Basis Function with $\gamma = 0.5$ \\
        \textbf{Penalization:} $C = 1.5$ \\
        \textbf{Model:} Hinge loss with intercept \\
        \textbf{Multiclass approach:} One-vs-Rest \\
        \textbf{Solver:} SMO ($500$ iterations max, \num{1e-5} tolerance)\\
        \textbf{Image Processing:} Unsigned Histograms of Oriented Gradients, 9 bins, $8$x$8$ cells, $2$x$2$ blocks, $L2$ normalization\\
        \textbf{Data Augmentation:} Horizontal Mirroring \\
    \end{footnotesize}
\end{center}
% ------------------------------------------------------------------------------
% ------------------------------------------------------------------------------
\section{Methodology}
\label{sec:methodology}

\paragraph{Visualizing the data}

The whitening used on the images makes it difficult to visualize the images but it is still possible to partially visualize them with a heatmap of one of the colour channels. This served as an important sanity check when reshaping the input vectors as \texttt{Julia} uses a column major ordering and it is therefore easy to incorrectly reshape the input vectors.

\vspace{-0.2cm}

\begin{figure}[H]
	\centering
	\subfloat[A dog]{\includegraphics[width=0.1\textwidth]{fig/dog}}
	\subfloat[A cat]{\includegraphics[width=0.1\textwidth]{fig/cat}}
	\subfloat[A boat]{\includegraphics[width=0.1\textwidth]{fig/boat}}
	\subfloat[A car]{\includegraphics[width=0.1\textwidth]{fig/car}}
	\subfloat[A horse]{\includegraphics[width=0.1\textwidth]{fig/horse}}
\end{figure}

While this preprocessing made visualization more difficult I believe it was a great help as it not only normalized the data, a very important step for SVMs to perform well (in fact whitening would probably have been a primordial step if it wasn't done for us!) but by looking at the images it seems that it accentuates edges, a very convenient side effect in our case.


\paragraph{Data augmentation and online learning}

Our dataset consists of only $5000$ images which is relatively small, it seemed to me that on the fly data augmentation could be a worthwhile feature as it is often a simple way to avoid overfitting in image classification. I therefore chose to create a \texttt{Dataset} type that acted like an array or iterator depending on the need, this enables for example the use of the \texttt{ImageData} type that on the fly creates new images resulting in a virtual dataset of size $160000$.
Of course if one is to use datasets this large simply solving the QP problem is intractable, even building the gram matrix would require \texttt{204.8 gigabytes} array. Clearly even if it was possible, manipulating it wouldn't be.

Because examples are created on the fly I first concentrated on online learning methods for SVMs. Stochastic gradient methods are a perfect fit and if we remember that we aren't actually solving an optimization problem but trying to find a decision function that generalizes well then SGD, while a terrible optimization methods, has excellent real world performances in machine learning because of its regularizing effect.

I considered implementing \texttt{Pegasos} of \cite{Yoram} but the Kernel version of \texttt{Pegasos} is a second class citizen and doesn't have the same properties as the linear version. Another good model I considered but didn't implement because of a lack of time was \texttt{LaSVM} of \cite{Bordes2005}.
Online learning is very attractive but because prediction for SVMs requires support vectors the set of support vectors to store can potentially grow unbounded, making our efforts vain (in practice this doesn't happen), I therefore chose to implement the method of \cite{Wang2012} which is a variant of SGD which keeps a fixed budget of support vectors. While we are under the budget the algorithm behaves exactly like \texttt{Pegasos} but once the budget is reached a "Budget Maintenance" routine is used, the authors proposes three variants: naively dropping the support vector corresponding to the smallest $\alpha$, projecting a support vector on the other support vectors and merging support vectors. I chose to implement the merging variants which limits the algorithms to Gaussian kernels. It is implemented as \texttt{BGSD} in the \texttt{SVM} module.
While the algorithm works (or at least appears to) I quickly deprecated it for several reasons:
\begin{itemize}
	\itemsep0em 
	\item I finally decided to use a milder augmentation that fits in memory.
	\item All these SGD models don't have an intercept, in my tests the intercept was important.
	\item Feature engineering reduced the dimension from $3072$ to $324$, therefore storing support vectors was less a problem.
\end{itemize}

\paragraph{"Vanilla" SVMs}

For quick prototyping and making sure my results were correct I decided to implement a "vanilla" SVM by just specifying the quadratic program and passing it to a QP solver. This method is implemented by \texttt{Vanilla} ($C$ formulation of the SVM problem) and \texttt{NuVanilla} ($\nu$ formulation of the SVM problem). I first used the great \texttt{Convex} package for \texttt{Julia} to specify the problem and solve it with \texttt{SCS} unfortunately this was extremely slow ($30$ minutes for $5000$ images in One-vs-One, unbearably slow for $10000$ images or in One-vs-Rest), while I know this is relatively quick in \texttt{CVX} for \texttt{Matlab} and \texttt{Python}. I then realized that \texttt{Convex} is able to solve much more general problems than \texttt{CVX} and was therefore first recasting the problem as a SOCP program and then solving that SOCP program in \texttt{SCS}. Our problem has a much simpler structure as it is a QP program, I therefore ditched \texttt{Convex} for the lower level \texttt{MathProgBase}. Unfortunately \texttt{Julia} doesn't have bindings to a dedicated QP solver, so I at first opted for \texttt{Ipopt} which brought down the time to 3:23 minutes for $5000$ images in OvO. Finally, while it is not an open-source package I settled for \texttt{Mosek} which has a dedicated QP solver can solve the same problem in 0:52 minutes. \texttt{Mosek} is included in the docker image or a trial licence can be very simply obtained (unlike \texttt{Gurobi}).


\paragraph{SMO}

I also decided to implement the \texttt{SMO} algorithm of \cite{Platt1999}, based on the first exercise of Homework 2, with the full heuristic. The \texttt{SMO} algorithm has two extremely appealing features. First it is extremely fast ($0$:$11$) by cleverly exploiting the structure of the problem (i.e. the sparsity of $\alpha$) and therefore concentrates its efforts only on the non-bound coefficients, i.e. the hard to classify examples, and mostly ignores the bound coefficients, i.e. the easy to classify examples. Secondly, by only considering two coefficients at once and jointly optimizing them we free ourselves from the need to store the whole gram matrix $K$ and we only need to know $K_{ii}, K_{ij}$ and $K_{jj}$. To keep good performances it is, however, important to cache some of the matrix $K$ (in practice the $N$ last used columns). In my implementation I didn't implement caching because of a lack of time and because keeping the whole matrix was at our size possible and greatly speeds the algorithm. Implementing caching is, however, a trivial change.

\paragraph{Histograms of oriented gradients}

It very quickly became clear that working only with the raw features wasn't going to be enough, it is not a surprise that feature engineering is the most important part of an image challenge. I decided to implement the HoG algorithm of \cite{Dalal2010} as it was the state of the art in image classification before CNNs. I implemented the original algorithm following the recommendations of the authors and used unsigned angles, $9$ bins, $8$x$8$ cells and $2$x$2$ blocks $L2$-normalized. I also, as recommended, used a bilinear interpolation to divide the votes into bins and reduce aliasing. This gives us $324$ features.

I tried using PCA (in module \texttt{PCA}) to further reduce dimensions but the impact on performance wasn't significative (cross-validation indicated that $n = 100$ was optimal but barely). I therefore opted not to use it.

Because our features are normalized histograms no further processing like whitening was needed (implemented in module \texttt{Preprocessing}).

\paragraph{Multiclass}

Our problem is a multiclass problem while SVM in its standard formulation is a binary classification algorithm. I completely ignored multi-class SVM formulations as they were too specific and the literature didn't indicate any improvements. I implemented both one-vs-one and one-vs-rest as wrappers (\texttt{OneVsOne} and \texttt{OneVsRest}). I first concentrated on one-vs-one as the literature didn't seem to designate a superior method, and one-vs-one had several advantages:
\begin{itemize}
	\itemsep0em 
	\item It is much faster. While we build $\frac{C (C-1)}{2}$ classifiers instead of $C$ those classifiers are much smaller and SVM scales quadratically.
	\item It doesn't suffer from class imbalance
	\item Each small classifier has its own parameters and can therefore adapt more.
\end{itemize} 

But after comparing with one-vs-rest it appeared that the latter had an accuracy advantage.

% ------------------------------------------------------------------------------
% ------------------------------------------------------------------------------
\subsection{Results}
\label{sec:results}

Using the classifier described in the introduction, I reached \texttt{0.60800} on the public leaderboard and \texttt{0.59600} on the private leaderboard. Looking at the progression of my submissions one can clearly see a first jump in performance from around \texttt{0.2} to \texttt{0.43} when I introduced \texttt{hog} as a preprocessing step and another one to the final score when I noticed that my \texttt{Vanilla} SVM had an error in the intercept. The leaderboard submission was done by letting \texttt{SCS} running overnight but my script uses the changes described before for a very significant speedup. Results should be identical (except with SMO, some randomness in the solution is expected). I do \texttt{0.02} better on the public leaderboard and \texttt{0.005} worse on the private, indicating some overfit. I chose to showcase my new methods in the script as I believe \texttt{0.591} in minutes is a more impressive result than \texttt{0.596} in more than a day.

% ------------------------------------------------------------------------------
% ------------------------------------------------------------------------------
\clearpage
\appendix
\section{Notes on the implementation}
\label{sec:implementation}

\paragraph{Design choices}

I have tried as much as possible given the time constraints to write composable and general yet performant code. It is possible to build arbitrary \texttt{Kernels} (a sum or product of \texttt{Kernel}s is recognized as a \texttt{Kernel}) as well as to layer classifiers. I have also when possible made use of threading to speed upd computations. When a common use case is recognized by the type dispatcher, like computing the gram matrix of a Gaussian kernel, specialized hand optimized code is called.
Many modules are available and implements commonly used functions like \texttt{CrossVal}, importing and exporting in \texttt{Utils} or image manipulation in \texttt{ImageUtils}.

\paragraph{Possible Improvements}

Currently while my code makes use of the fact that gram matrices can be shared during training by classifiers the implementation is not complete and pretty fragile, it would be a worthwhile and easy improvements to extend the implementation (for \texttt{OneVsOne} for example) but one must be careful to check for edge cases when matrices are too large and implementing fallbacks to on-the-fly or online methods.

It would also be a good idea to also do data augmentation on the test images and do an aggregation of the predictions on the test data to decide the final prediction.

I think accuracy could have been slightly improved by implementing Platt scaling from \cite{Platt1999a} as a calibrated output from \texttt{OneVsRest} would have made ensembling possible. I think using a special form of bagging in \texttt{OneVsOne} could have improved accuracy as well as speed by balancing datasets.

In the end I think any significative improvements could have only come from more feature engineering or more image augmentation (easy with SMO) as our classifier as a train error of less than $1$\% which indicates that we are clearly overfitting.

\paragraph{Methods implemented but not used}

I also implemented several methods that finally didn't see any use but could potentially be used either to improve performance or accuracy:
\begin{enumerate}
	\itemsep0em 
	\item A \texttt{Bagging} wrapper to perform bagging.
	\item An \texttt{IncompleteChol} method in the module \texttt{Approximations}.
	\item An extremely naive \texttt{KNN}.
\end{enumerate}

% ------------------------------------------------------------------------------
\hr{}  % Horizontal line, like <hr> in HTML
% XXX remove if it is ugly or take too much space

\appendix
% % ------------------------------------------------------------------------------
\label{sub:references}
\printbibliography

% ------------------------------------------------------------------------------
\hr{}

\end{document}