#!/bin/bash
DOCKERID="$(docker run -v $PWD:/home/kernel -it -d aussetg/julia-mosek)"
docker exec $DOCKERID bash -c 'cd /home/kernel; julia --color=yes start.jl'
