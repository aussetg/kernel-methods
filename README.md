# Repository for the course on Kernel Methods - 2017

## Guillaume Ausset - Master M.A.S.H

[![documentation](https://img.shields.io/badge/docs-latest-blue.svg)](https://aussetg.gitlab.io/kernel-methods)
[![build status](https://gitlab.com/aussetg/kernel-methods/badges/master/build.svg)](https://gitlab.com/aussetg/kernel-methods/commits/master)

This project was done using `Julia v0.5`. You can clone this repository but should not forget to use [`Git LFS`][1] to get the data.

### Dependencies:

* `Julia v0.5`
* Iterators
* MathProgBase
* [Mosek](https://www.mosek.com/introduction/try-mosek)
* Distances
* Optim
* ProgressMeter

A docker image with all dependencies installed is available:

```
docker pull docker pull aussetg/julia-mosek:latest
docker run -it --rm docker pull aussetg/julia-mosek:latest
```

### How to run the script

* Make sure data/ contains the input files, either by putting them manually or by cloning this repo with `git lfs clone https://gitlab.com/aussetg/kernel-methods.git`, or if already clone running `git lfs pull`
* If you have all dependencies (you can install them with `install_deps.sh`) you can directly run either `start.sh` or `julia start.jl`. If you opt for the latter, you can speed up training by setting the `JULIA_NUM_THREADS` environment variable.
* If you don't have all the dependencies or don't want to install then you can use Docker and run the supplied container with `start_with_docker.sh`.

Structure of this Repository:

```
├── data                # The necessary data files, stored with Git LFS
│   └── Xtr.csv
│   ...
├── doc                 # Files to generate the documentation
│   └── src
│   └── make.jl
├── notebooks
│   └── Example.ipynb   # Jupyter notebook serving as an example for the code
├── report
│   └── Report.tex      # The final report for the course
│   └── Report.pdf
├── src                 # The source files for all the implemented methods
│   └── Data.jl
│   ...
├── test
│   └── test.jl         # Tests
├── LICENSE
├── README.md           # This file
└── REQUIRE
```

[1]: https://git-lfs.github.com](https://git-lfs.github.com
