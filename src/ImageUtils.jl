module ImageUtils

export toImage, grayscale, mirror, hog, imfilter, gradient, rotate90

function grayscale(image::Array{Float64,3}, method = 1)
  if method == 2
    im = 0.333.*image[:,:,1] + 0.333.*image[:,:,2] + 0.333.*image[:,:,3]
  else
    im = 0.2989.*image[:,:,1] + 0.5870.*image[:,:,2] + 0.1140.*image[:,:,3]
  end
  return im
end

function toImage(v::Vector{Float64}, h::Int, w::Int)
  temp = reshape(v, (w, h, 3))
  im = zeros(Float64, (h, w, 3))
  for i = 1:3
    im[:,:,i] = rotl90(temp[:,:,i])
  end
  return im
end

function mirror(image::Matrix{Float64})
  return image[:,end:-1:1]
end

function mirror(image::Array{Float64,3})
  return image[:,end:-1:1,:]
end


"""
Applies a filter to the image.
"""
function imfilter(image::Matrix{Float64}, filter::Matrix{Float64})
  (fxsize, fysize) = size(filter)
  fxsize = div(fxsize-1, 2)
  fysize = div(fysize-1, 2)
  (n, m) = size(image)
  temp = zeros(Float64,(n+2*fxsize, m+2*fysize))
  temp[(fxsize+1):(n+fxsize),(fysize+1):(n+fysize)] = image
  out = zeros(Float64, (n,m))
  for j = 1:m
    for i = 1:n
      out[i,j] = sum(filter .* temp[i:(i+2*fxsize),j:(j+2*fysize)])
    end
  end
  return out
end

function imfilter(image::Matrix{Float64}, filter::Vector{Float64})
  return imfilter(image, filter[:,:])
end

function gradient(image::Matrix{Float64})
  (n, m) = size(image)
  dx = zeros(Float64, (n,m))
  dy = zeros(Float64, (n,m))
  for i in 2:(m-1)
    dx[:,i] = 0.5*(image[:,i+1] - image[:,i-1])
  end
  dx[:,1] = image[:,2] - image[:,1]
  dx[:,m] = image[:,m] - image[:,m-1]
  for i in 2:(n-1)
    dy[i,:] = 0.5*(image[i+1,:] - image[i-1,:])
  end
  dy[1,:] = image[2,:] - image[1,:]
  dy[n,:] = image[n,:] - image[n-1,:]
  return dx, dy
end

function makeHistogram(angles, magnitudes, bins)
  # Compute the bin size
  binSize = pi / bins

  # Make the angles unsigned
  angles[angles .< 0] += pi

  H = zeros(Float64,bins) # Histogram

  # We put values into bins by their magnitude
  leftBoundary = Vector{Int}(floor.(angles / binSize))

  # Distribute according to distance to bin left boundary
  rightPortions = abs(angles - leftBoundary*binSize)/binSize
  leftPortions = 1.0 - rightPortions

  for i = 1:bins
    idx = find(x -> (i-1)*binSize <= x < i*binSize, angles)

    # Distribute magnitude according to left distance to centers
    H[i] += dot(leftPortions[idx], magnitudes[idx])

    # Distribute magnitude according to distance to right centers
    H[i % bins + 1] += dot(rightPortions[idx], magnitudes[idx])
  end
  return H
end

"""
Compute the Histogram of Oriented Gradients, Black and white
"""
function hog(image::Matrix{Float64}, cellsize = (8, 8), blocksize = (2, 2))
  # As per the paper:
  # 8x8 pixel cells
  # Block size of 2x2 cells
  # 50% overlap between blocks
  (cellsizev, cellsizeh) = cellsize
  (blocksizev, blocksizeh) = blocksize
  nbins = 9 # 9-bin histogram
  (n, m) = size(image)
  # Compute Gradients
  filter = [-1.0 .0 1.0]
  dx, dy = gradient(image)
  # To angle/magnitude
  angles = atan2.(dx, dy)
  magnitudes = hypot.(dy, dx)
  # Build histograms
  nvcells = Int(m//cellsizev)
  nhcells = Int(n//cellsizeh)
  histograms = zeros(Float64, (nvcells, nhcells, nbins))

  # We build the histogram for each cell
  for row = 0:(nvcells - 1)

    rowOffset = (row * cellsizev) + 1

    for col = 0:(nhcells - 1)

        colOffset = (col * cellsizeh) + 1

        rowIdx = rowOffset:(rowOffset + cellsizev - 1)
        colIdx = colOffset:(colOffset + cellsizeh - 1)

        cellAngles = angles[rowIdx, colIdx]
        cellMagnitudes = magnitudes[rowIdx, colIdx]

        # Compute the cell's histogram
        histograms[row + 1, col + 1, :] = makeHistogram(cellAngles[:], cellMagnitudes[:], nbins)
    end
  end

  ### Block Normalization
  features = Vector{Float64}((nvcells-1)*(nhcells-1)*nbins*blocksizev*blocksizeh)
  i = 0
  for row = 1:(nvcells - 1)
    for col = 1:(nhcells - 1)

      # Get the histograms for the cells in this block.
      blockHists = histograms[row:(row + blocksizev - 1), col:(col + blocksizeh - 1), :]

      m = norm(blockHists[:]) + 1e-9
      normalized = blockHists / m

      features[(i*nbins*blocksizev*blocksizeh + 1):((i+1)*nbins*blocksizev*blocksizeh)] = normalized[:]

      i += 1
    end
  end
  return features
end

"""
Compute the Histogram of Oriented Gradients, color
"""
function hog(image::Array{Float64,3}, cellsize = (8, 8), blocksize = (2, 2))
  # As per the paper:
  # 8x8 pixel cells
  # Block size of 2x2 cells
  # 50% overlap between blocks
  (cellsizev, cellsizeh) = cellsize
  (blocksizev, blocksizeh) = blocksize
  nbins = 9 # 9-bin histogram
  (n, m, c) = size(image)
  # Compute Gradients
  filter = [-1.0 .0 1.0]
  dxs = Array{Float64,3}((n, m, c))
  dys = Array{Float64,3}((n, m, c))
  for channel = 1:c
    dxs[:,:,channel], dys[:,:,channel] = gradient(image[:,:,channel])
  end
  dx = reshape(mapslices(maximum, dxs, 3), (n,m))
  dy = reshape(mapslices(maximum, dys, 3), (n,m))
  # To angle/magnitude
  angles = atan2.(dx, dy)
  magnitudes = hypot.(dy, dx)
  # Build histograms
  nvcells = Int(m//cellsizev)
  nhcells = Int(n//cellsizeh)
  histograms = zeros(Float64, (nvcells, nhcells, nbins))

  # We build the histogram for each cell
  for row = 0:(nvcells - 1)

    rowOffset = (row * cellsizev) + 1

    for col = 0:(nhcells - 1)

        colOffset = (col * cellsizeh) + 1

        rowIdx = rowOffset:(rowOffset + cellsizev - 1)
        colIdx = colOffset:(colOffset + cellsizeh - 1)

        cellAngles = angles[rowIdx, colIdx]
        cellMagnitudes = magnitudes[rowIdx, colIdx]

        # Compute the cell's histogram
        histograms[row + 1, col + 1, :] = makeHistogram(cellAngles[:], cellMagnitudes[:], nbins)
    end
  end

  ### Block Normalization
  features = Vector{Float64}((nvcells-1)*(nhcells-1)*nbins*blocksizev*blocksizeh)
  i = 0
  for row = 1:(nvcells - 1)
    for col = 1:(nhcells - 1)

      # Get the histograms for the cells in this block.
      blockHists = histograms[row:(row + blocksizev - 1), col:(col + blocksizeh - 1), :]

      m = norm(blockHists[:]) + 1e-3
      normalized = blockHists / m

      features[(i*nbins*blocksizev*blocksizeh + 1):((i+1)*nbins*blocksizev*blocksizeh)] = normalized[:]

      i += 1
    end
  end
  return features
end

doc"""
Rotate an image by 90° clockwise.
"""
function rotate90(image::Array{Float64})
  (n, m, channels) = size(image)
  temp = Array{Float64, 3}((n, m, channels))
  for c in channels
    temp[:,:,c] = rotl90(image[:,:,c])
  end
  return temp
end

function rotate90(image::Matrix{Float64})
  return rotl90(image)
end

doc"""
Rotate an image by ``\theta`` clockwise.
"""
function rotate(image::Array{Float64}, Θ::Float64)
end

end
