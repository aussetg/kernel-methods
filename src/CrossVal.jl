module CrossVal

using Data, SVM, ProgressMeter

export CV

function makefolds(x::AbstractArray, n::Int)
    s = length(x) / n
    [x[round(Int64, (i-1)*s)+1:min(length(x),round(Int64, i*s))] for i=1:n]
end

"""
  CV(s::KSVM, x::Dataset, nfolds = 3)

Perform 'nfolds' crossvalidation.
"""
function CV(s::KSVM, x::Dataset, nfolds = 3; threading = true)
  lbl = labels(x)
  img = data(x)
  n = length(lbl)
  idx = randperm(n)
  acc = Vector{Float64}()
  folds = makefolds(idx, nfolds)
  if !threading
    progressBar = Progress(nfolds, 1, "CV:", 50)
  else
    println("Progress bar disabled when threading is used.")
  end
  Threads.@threads for i = 1:nfolds
    idxf = folds[i]
    idxnf = vcat([folds[j] for j = 1:nfolds if j != i]...)
    xf = typeof(x)(img[:,idxf], lbl[idxf])
    xnf = typeof(x)(img[:,idxnf], lbl[idxnf])
    sf = deepcopy(s)
    fit(sf, xnf; threading = false, progress = false)
    preds = predict(sf, xf)
    push!(acc, mean(preds .== lbl[idxf]))
    if !threading
      next!(progressBar)
    end
  end
  return mean(acc)
end

end
