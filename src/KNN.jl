module KNN

using Data, Kernels

export KKNN, Naive, train, predict

abstract KKNN

"""
The worst possible KNN implementation: just compute all distances.
"""
immutable Naive
  kernel::Kernel
  examples::Dataset
  K::Int
end

"""
There is nothing to train
"""
function train(knn::Naive, x::Dataset)
end

function predict(s::Naive, x::Dataset)
  n = length(x)
  m = length(knn.examples)
  preds = Vector{Int}(n)
  idx = Vector{Int}(m)
  for i = 1:n
    dists = Vector{Int}(m)
    Threads.@threads for j = 1:m
      dists[j] = k(knn.kernel, x[i,1], x[i,1]) + k(knn.kernel, knn.examples[j,1], knn.examples[j,1]) - k(knn.kernel, knn.examples[j,1], x[i,1])
    end
    sortperm!(idx, dists; alg=QuickSort)
    preds[i] = vote(idx[1:knn.K])
  end
end

"""
A helper function to tally predictions i.e. take a majority votes.
"""
# TODO: Redundant. Also in SVM
function vote(votes)
  t = Dict{Int,Int}()
  for v in votes
    if !haskey(t, v)
      t[v] = 0
    end
    t[v] += 1
  end
  return reduce((x, y) -> t[x] >= t[y] ? x : y, keys(t))
end

end
