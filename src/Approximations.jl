module Approximations

using Kernels, Data
export IncompleteChol

doc"""
    IncompleteChol(K::Kernel, x::Dataset[, eta]) -> V::Matrix{Float64}

Compute the incomplete cholesky decomposition of the gram matrix of the kernel K with precision eta. If eta is not provided eta = 1e-2 is used. Elements of the gram matrix are lazily evaluated when needed, the whole Gram matrix is never entirely constructed. Return P the permutation matrix, G a lower triangular matrix and M the rank of the approximation such that:
``|P*K*P' - G*G'| < eta``
P is given as reordering vector.
See page 20.
"""
@fastmath function IncompleteChol(K::Kernel, x::Dataset, eta = 1e-6)
  N = length(x)
  i = 1
  K = spzeros(N,N)
  P = collect(1:N)
  G = speye(Float64,N)
  computed = zeros(Int,N) # On va garder en mémoire les colonnes déja calculées
  for k = 1:N
    G[k,k] = k(x[k,1],x[k,1])
  end
  while sum(diag(G)[i:N]) > eta
    # Find new best element
    j = indmax(diag(G)[i:N]) + i - 1
    # Update P
    P[[i,j]] = P[[j,i]]
    # Construit les éléments dont on va avoir besoin
    # TODO: Faut t'il verifier si il existent deja avant de les recalculer et
    # TODO: potentiellement les ecraser ?
    if computed[i] == 0
      for k = 1:N
        K[k,i] = k(x[k,1],x[i,1])
        K[i,k] = K[k,i]
      end
    end
    computed[i] = 1
    if computed[j] == 0
      for k = 1:N
        K[k,j] = k(x[k,1],x[j,1])
        K[j,k] = K[k,j]
      end
    end
    computed[j] = 1
    # Permute elements i and j in K
    K[:,i], K[:,j] = K[:,j], K[:,i] # En fait on peut utiliser K[:,[j,i]]
    K[i,:], K[j,:] = K[j,:], K[i,:]
    # Update G
    G[i,1:i], G[j,1:i] = G[j,1:i], G[i,1:i]
    G[i,i] = sqrt(diag(G)[j]) # The paper uses K[i,i]: doesn't work
    # ith column of G
    G[(i+1):N, i] = (K[(i+1):N, i] - G[(i+1):N, 1:(i-1)] * G[i, 1:(i-1)])/G[i,i]
    # Update only diagonal elements
    for k = (i+1):N
      G[k,k] = k(x[k,1],x[k,1]) - sum(G[k,1:i] .^ 2)
    end
    i = i+1
  end
  return P, full(G[:,1:(i-1)]), (i-1)::Int
end
# TODO: Problème avec P, on ne remplit pas P*K*P' - G*G' < eps ?!
# TODO: On a  K - G*G' < eps

end
