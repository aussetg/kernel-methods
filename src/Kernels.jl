module Kernels

using Distances

export Kernel, k, Parameter, RealParam, PositiveParam, NegativeParam, BoundedParam, LinearKernel, GaussianKernel, LaplaceKernel, SumKernel, ProdKernel, ScaleKernel, gram

"""
A Kernel is a definite positive function, to ensure that all functions passed to our algorithms are indeed kernels we will constrain them to be of `Kernel` type. This also enable us to define functions to combine kernels as well as a way to define parameters to optimize them by cross validation.
"""
abstract Kernel

"""
We only require a `Kernel` to have an evaluation function.
"""
function k(::Kernel,x,y)
end

"""
Kernels have hyperparameters, we therefore define a `Parameter` type to validate our parameters and enable optimization of our hyperparameters in the future.
"""
abstract Parameter

doc"""
A `Realparam` is a parameter ``p \in \mathbb{R}``
"""
type RealParam <: Parameter
  p::Float64
  RealParam(p) = new(p)
end

doc"""
A `BoundedParam` is a parameter ``p \in [u, l]``
"""
type BoundedParam <: Parameter
  l::Float64
  u::Float64
  p::Float64
  BoundedParam(l,u) = u < l ? error("Upper bound must be greater than the lower bound.") : new(l,u,0.5*(l+u))
end

doc"""
A `PositiveParam` is a parameter ``p \in \mathbb{R}^+``
"""
type PositiveParam <: Parameter
  p::Float64
  PositiveParam(p) = p < 0 ? error("Parameter must be positive.") : new(p)
end

doc"""
A `NegativeParam` is a parameter ``p \in \mathbb{R}^-``
"""
type NegativeParam <: Parameter
  p::Float64
  NegativeParam(p) = p > 0 ? error("Parameter must be negative.") : new(p)
end

"""
The linear kernel.
"""
type LinearKernel <: Kernel
  c::PositiveParam
  LinearKernel() = new(PositiveParam(0.0))
  LinearKernel(c) = new(PositiveParam(c))
end

doc"""
The kernel function for linear kernel is ``x y``
"""
function k(g::LinearKernel, x, y)
  return dot(x,y) + g.c.p
end

"""
The Gaussian RBF is a translation invariant kernel accepting a single positive parameter.
"""
type GaussianKernel <: Kernel
  γ::RealParam
  d::Metric
  GaussianKernel() = new(RealParam(1.0), Euclidean())
  GaussianKernel(γ) = new(RealParam(γ), Euclidean())
  GaussianKernel(γ, d) = new(RealParam(γ), d)
end

doc"""
The kernel function for an RBF of parameter ``\gamma > 0`` is ``e^{-\gamma * \lVert x - y \rVert^2}``
"""
function k(g::GaussianKernel, x, y)
  r = evaluate(g.d, x, y)
  return exp(-g.γ.p*r^2)
end

"""
The Laplace kernel is a translation invariant kernel accepting a single positive parameter.
"""
type LaplaceKernel <: Kernel
  γ::PositiveParam
  d::Metric
  LaplaceKernel() = new(PositiveParam(1.0), Cityblock())
  LaplaceKernel(γ) = new(PositiveParam(γ), Cityblock())
  LaplaceKernel(γ, d) = new(PositiveParam(γ), d)
end

doc"""
The kernel function for a Laplace kernel of parameter ``\gamma > 0`` is ``e^{-\gamma * \lVert x - y \rVert}``
"""
function k(g::LaplaceKernel, x, y)
  r = evaluate(g.d, x, y)
  return exp(-g.γ.p*r)
end

"""
Kernels are stable by addition, we can define the sum of two kernels
"""
type SumKernel <: Kernel
  g::Kernel
  h::Kernel
end

function k(g::SumKernel, x, y)
  return k(g.g, x, y) + k(g.h, x, y)
end

"""
Kernels are stable by multiplication, we can define the product of two kernels
"""
type ProdKernel <: Kernel
  g::Kernel
  h::Kernel
end

function k(g::ProdKernel, x, y)
  return k(g.g, x, y) * k(g.h, x, y)
end

"""
Kernels are stable by positive scaling, we can define the scaling of a kernel
"""
type ScaleKernel <: Kernel
  c::PositiveParam
  g::Kernel
  ScaleKernel(c::PositiveParam, g::Kernel) = new(c, g)
  ScaleKernel(c::Float64, g::Kernel) = new(PositiveParam(c), g)
end

function k(g::ScaleKernel, x, y)
  return g.c.p * k(g.g, x, y)
end

Base.:+(g::Kernel, h::Kernel) = SumKernel(g,h)
Base.:*(g::Kernel, h::Kernel) = ProdKernel(g,h)

function gram(g::Kernel, x::Matrix{Float64}, y::Matrix{Float64})
  n = size(x)[1]
  m = size(y)[1]
  K = Matrix{Float64}(n,m)
  # Add inbounds, don't duplicate work, parallelize?
  for i in 1:n
    for j in 1:m
      K[i,j] = k(g, x[i,:], y[j,:])
    end
  end
  return K
end

function gram(g::Kernel, x::Matrix{Float64}, y::Vector{Float64})
  return gram(g, x, y[:,:])
end

"""
Optimized Gaussian Kernel Gram computation.
"""
function gram(g::GaussianKernel, x::Matrix{Float64}, y::Matrix{Float64})
  if typeof(g.d) <: Euclidean
    K = pairwise(SqEuclidean(), x, y)
    γ = g.γ.p
    K = exp.(-γ .* K)
  else
    K = pairwise(g.d, x, y)
    γ = g.γ.p
    K = exp.(-γ .* K.^2)
  end
  return K
end

function gram(g::GaussianKernel, x::Matrix{Float64}, y::Vector{Float64})
  if typeof(g.d) <: Euclidean
    K = pairwise(SqEuclidean(), x, y[:,:])
    γ = g.γ.p
    K = exp.(-γ .* K)
  else
    K = pairwise(g.d, x, y[:,:])
    γ = g.γ.p
    K = exp.(-γ .* K.^2)
  end
  return K
end

function gram(g::GaussianKernel, x::Matrix{Float64})
  if typeof(g.d) <: Euclidean
    K = pairwise(SqEuclidean(), x)
    γ = g.γ.p
    K = exp.(-γ .* K)
  else
    K = pairwise(g.d, x)
    γ = g.γ.p
    K = exp.(-γ .* K.^2)
  end
  return K
end

"""
Optimized Laplace Kernel Gram computation.
"""
function gram(g::LaplaceKernel, x::Matrix{Float64}, y::Matrix{Float64})
  K = pairwise(g.d, x, y)
  γ = g.γ.p
  K = exp.(-γ .* K)
  return K
end

function gram(g::LaplaceKernel, x::Matrix{Float64}, y::Vector{Float64})
  K = pairwise(g.d, x, y[:,:])
  γ = g.γ.p
  K = exp.(-γ .* K)
  return K
end

function gram(g::LaplaceKernel, x::Matrix{Float64})
  K = pairwise(g.d, x)
  γ = g.γ.p
  K = exp.(-γ .* K)
  return K
end

end
