module Data

using Iterators

export Dataset, start, next, done, ImageData, getindex, data, labels, VanillaDataset

"""
A dataset is simply an iterable data-structure that contains our (x,y) points.
"""
abstract Dataset

"""
As an iterable we need to be able to find the first point
"""
function Base.start(::Dataset)
end
"""
To advance in the structure
"""
function Base.next(::Dataset, state)
end
"""
And to know when we are done.
"""
function Base.done(::Dataset, state)
end

"""
We need to know the length to make predictions.
"""
function Base.length(::Dataset)
end

function data(::Dataset)
end

function labels(::Dataset)
end

immutable VanillaDataset <: Dataset
  images::Matrix{Float64} # d*n , we use column major ordering
  labels::Vector{Int}
  VanillaDataset(images::Matrix{Float64}) = new(images)
  VanillaDataset(images::Matrix{Float64}, labels::Vector{Int}) = new(images, labels)
end

function Base.start(S::VanillaDataset)
  return 1
end

function Base.next(S::VanillaDataset, state)
  return Tuple{Vector{Float64}, Int}((S.images[:,state], S.labels[state])), state+1
end

function Base.done(S::VanillaDataset, state)
  return state > length(S.labels)
end

function Base.getindex(S::VanillaDataset, i::Int)
  return Tuple{Vector{Float64}, Int}((S.images[:,i], S.labels[i]))
end

function Base.getindex(S::VanillaDataset, i::Int, j::Int)
  if j == 1
    r = S.images[:,i]
  else
    r = S.labels[i]
  end
  return r
end

function Base.length(S::VanillaDataset)
  size(S.images)[2]
end

function data(S::VanillaDataset)
  S.images
end

function labels(S::VanillaDataset)
  S.labels
end

"""
ImageData is a particular type of Dataset that includes data augmentation.
"""
immutable ImageData <: Dataset
 images::Matrix{Float64} # d*n , we use column major ordering
 labels::Vector{Int}
 index::Matrix{Int} # Deterministic index of all augmentations
 ImageData(images, labels) = new(images, labels, hcat(map(collect,collect(product(1:size(images,1),1:2,1:4,1:4)))...)')
 ImageData(images) = new(images, Vector{Int}(), hcat(map(collect,collect(product(1:size(images,1),1:2,1:4,1:4)))...)')
end

"""
We simply define the start of the DataSet as the first non augmented image.
"""
function Base.start(S::ImageData)
  return 1
end
"""
Our data augmentation includes horizontal mirroring and random translations. We extract 28x28 patches from our 32x32 images. We obtain a 4x4x2 = 32x data augmentation. On our 5000 image dataset this result in a virtual 160000 images data set.
"""
function Base.next(S::ImageData, state)
  idx = S.index[:,state]
  pos = idx[1]
  image = reshape(S.images[:,pos],(32,32,3))
  if idx[2] == 2
    image = image[end:-1:1,:,:]
  end
  ix = idx[3]
  jx = idx[4]
  image = image[ix:(ix+28-1),jx:(jx+28-1),:]
  return (Tuple{Vector{Float64}, Int}(image, S.labels[pos]), state+1)
end
"""
We need to know when to stop iterating.
"""
function Base.done(S::ImageData, state)
  return state > size(S.index,1)
end

function Base.length(S::ImageData)
  return size(S.index,1)
end

"""
Given the parameters for the augmentation we can find the image and labels.
"""
function Base.getindex(S::ImageData, i::Int)
  idx = S.index[i,:]
  pos = idx[1]
  image = reshape(S.images[:,pos],(32,32,3))
  if idx[2] == 2
    image = image[end:-1:1,:,:]
  end
  ix = idx[3]
  jx = idx[4]
  image = image[ix:(ix+28-1),jx:(jx+28-1),:]
  return Tuple{Vector{Float64}, Int}(image, S.labels[pos])
end

"""
Given the parameters for the augmentation we can find the image or label.
"""
function Base.getindex(S::ImageData, i::Int, j::Int)
  idx = S.index[i,:]
  if j == 1
    pos = idx[1]
    r = reshape(S.images[:,pos],(32,32,3))
    if idx[2] == 2
      r = r[end:-1:1,:,:]
    end
    ix = idx[3]
    jx = idx[4]
    r = r[ix:(ix+28-1),jx:(jx+28-1),:]
  else
    r = S.labels[pos]
  end
  return r
end

function data(S::ImageData)
  S.images
end

function labels(S::ImageData)
  S.labels
end

end
