module PCA

export pca, fit, transform

using Data

type pca
  m::Matrix{Float64}
  P::Matrix{Float64}
  k::Int
  pca() = new()
end

function __direct_pca(X::Matrix{Float64}, k::Int)
  # Substract the mean
  p, n = size(X)
  m = mapslices(mean, X, 2)
  B = X - m*ones((1,n))
  C = (B * B') / (n-1)
  U, S, V = svd(C)
  idx = ord[1:k]
  return m, V[:,idx], S[idx]
end

function __iterative_pca(x::Matrix{Float64}, k::Int)
end

function fit(pc::pca, X::Matrix{Float64}, k::Int)
  pc.k = k
  p, n = size(X)
  m = mapslices(mean, X, 2)
  B = X - m*ones((1,n))
  C = (B * B') / (n-1)
  U, S, V = svd(C)
  pc.m = m
  pc.P = U*diag(S.^-1)
  return pc
end

function transform(pc::pca, x::Dataset)
  if (typeof(x) != VanillaDataset)
    error("Can't transform a non VanillaDataset. Please use a Pipeline.")
  end
  X = data(x)
  lbl = labels(x)
  X = (X'*pc.P)[1:pc.k,:]
  return VanillaDataset(X, lbl)
end

function transform(p::pca, X::Matrix{Float64})
  return p.P * X
end

end
