module kernel-methods

export Data, Kernels, SVM

include("Data.jl")
include("Kernels.jl")
include("SVM.jl")

end
