module SVM

using Kernels
using Data
using Iterators
using Mosek
using MathProgBase
using Optim
using ProgressMeter

export KSVM, fit, predict, Vanilla, NuVanilla, SMO, BSGD, OneVsOne, OneVsRest

"""
An abstract type from which all the SVMs will inherit. Must have a `fit` and a `predict` function.
"""
abstract KSVM

function fit(s::KSVM, x::Dataset; threading = false)
  println("Please implement a specific instance of `fit` for $(typeof(s))")
end

function predict(s::KSVM, x::Dataset)
  println("Please implement a specific instance of `predict` for $(typeof(s))")
end

function save(s::KSVM)
end

function load()
end

"""
A "vanilla" SVM that directly optimizes the dual of the SVM problem using standard techniques. Make use of the `Convex` package to express the problem in a readable and straightforward way. Simplest and slowest option.
"""
type Vanilla <: KSVM
  kernel::Kernel # Chosen kernel
  C::Float64 # Penalization in [0, ∞]
  loss::Symbol
  classes::Dict{Int,Int} # The two class labels
  sv::Matrix{Float64} # Support vectors
  sl::Vector{Int} # Support labels
  α::Vector{Float64} # Coefficients of the support vectors
  b::Float64 # Intercept
  solver::MathProgBase.SolverInterface.AbstractMathProgSolver
  Vanilla(g::Kernel, C::Float64) = new(g, C, :hinge, Dict{Int,Int}(), Matrix{Float64}(), Vector{Int}(), Vector{Float64}(), 0.0, MosekSolver("QUIET"))
  Vanilla(g::Kernel, C::Float64, loss::Symbol) = (loss != :hinge && loss != :sqhinge) ? error("Only support :hinge and :sqhinge losses.") : new(g, C, loss, MosekSolver("QUIET"), Dict{Int,Int}(), Matrix{Float64}(), Vector{Int}(), Vector{Float64}(), 0.0, MosekSolver("QUIET"))
  Vanilla(g::Kernel, C::Float64, loss::Symbol, solver::MathProgBase.SolverInterface.AbstractMathProgSolver) = (loss != :hinge && loss != :sqhinge) ? error("Only support :hinge and :sqhinge losses.") : new(g, C, loss,  Dict{Int,Int}(), Matrix{Float64}(), Vector{Int}(), Vector{Float64}(), 0.0, solver)
end

function regularize!(K::Matrix{Float64})
  K += 1e-5 .* eye(size(K,1))
end

function fit(s::Vanilla, x::Dataset; intercept = true, theading = false, precomputedK = Matrix{Float64}())
  examples = collect(x)
  # Extract labels
  lbl = Vector{Int}(map(x -> x[2], examples))
  # Get the classes
  classes = sort(unique(lbl), rev = true)
  if length(classes) != 2
    error("ERROR: Not a binary classification problem")
  end
  # Remap classes to 1 / -1 and remember the mapping
  s.classes[1] = classes[1]
  s.classes[-1] = classes[2]
  pos = find(x -> x == classes[1], lbl)
  neg = find(x -> x == classes[2], lbl)
  lbl[pos] = 1
  lbl[neg] = -1
  # Extract examples
  map!(x -> x[1], examples)
  examples = hcat(examples...)
  if isempty(precomputedK)
    K = gram(s.kernel, examples)
  else
    K = precomputedK
  end
  # Define the convex program
  n = length(lbl)
  α = zeros(Float64,n)
  c = -lbl
  if s.loss == :sqhinge
    K += n/s.C .* eye(Float64, n)
  end
  if !isposdef(K)
    regularize!(K)
  end
  #if s.loss == :hinge
  #  problem.constraints += (α <= s.C)
  #end
  A = vcat(ones(n)',-ones(n)', sparse(diagm(lbl)), -sparse(diagm(lbl)))
  b = vcat(0.0, 0.0, s.C*ones(Float64,n), zeros(Float64,n))
  m = quadprog(c, K, A, '<', b, -Inf, Inf, s.solver) # ,OutputFlag=0
  # We now compute b
  α = lbl .* m.sol # For "compatibility" with the rest of the world
  s.b = intercept ? mean(lbl - K*(α .* lbl)) : 0
  # Let's "trim" our support vectors
  idx = find(x -> x > 1e-8, α)
  s.α = α[idx]
  s.sv = examples[:,idx]
  s.sl = lbl[idx]
  return s
end

function predict(s::Vanilla, x::Dataset; raw = false)
  examples = data(x)
  K = gram(s.kernel, examples, s.sv)
  if raw
    lbl = K * (s.α .* s.sl) + s.b
  else
    lbl = sign(K * (s.α .* s.sl) + s.b)
    # We will break ties in the inlikely event they happen
    idx = find(x -> x == 0, lbl)
    lbl[idx] = 1
    map!(x -> s.classes[x], lbl)
  end
  return lbl
end

doc"""
Same as "vanilla" but using the ν formulation of SVMs instead of the C formulation.
A "vanilla" SVM that directly optimizes the dual of the SVM problem using standard techniques. Make use of the `Convex` package to express the problem in a readable and straightforward way. Simplest and slowest option.
``\nu \in [0,1] ``
"""
type NuVanilla <: KSVM
  kernel::Kernel # Chosen kernel
  ν::Float64 # Penalization in [0,1]
  classes::Dict{Int,Int} # The two class labels
  sv::Matrix{Float64} # Support vectors
  sl::Vector{Int} # Support labels
  α::Vector{Float64} # Coefficients of the support vectors
  b::Float64 # Intercept
  solver::MathProgBase.SolverInterface.AbstractMathProgSolver
  NuVanilla(g::Kernel, ν::Float64) = new(g, ν, Dict{Int,Int}(), Matrix{Float64}(), Vector{Int}(), Vector{Float64}(), 0.0, MosekSolver())
  NuVanilla(g::Kernel, ν::Float64, solver::MathProgBase.SolverInterface.AbstractMathProgSolver) = new(g, ν, Dict{Int,Int}(), Matrix{Float64}(), Vector{Int}(), Vector{Float64}(), 0.0, solver)
end

function fit(s::NuVanilla, x::Dataset; intercept = true, threading = false, precomputedK = Matrix{Float64}())
  examples = collect(x)
  # Extract labels
  lbl = Vector{Int}(map(x -> x[2], examples))
  # Get the classes
  classes = sort(unique(lbl), rev = true)
  if length(classes) != 2
    error("ERROR: Not a binary classification problem")
  end
  # Remap classes to 1 / -1 and remember the mapping
  s.classes[1] = classes[1]
  s.classes[-1] = classes[2]
  pos = find(x -> x == classes[1], lbl)
  neg = find(x -> x == classes[2], lbl)
  lbl[pos] = 1
  lbl[neg] = -1
  # Extract examples
  map!(x -> x[1], examples)
  examples = hcat(examples...)
  if isempty(precomputedK)
    K = gram(s.kernel, examples)
  else
    K = precomputedK
  end
  # Define the convex program
  if !isposdef(K)
    regularize!(K)
  end
  n = length(lbl)
  α = zeros(Float64,n)
  c = zeros(n)
  A = vcat(ones(n)',-ones(n)', -lbl', sparse(diagm(lbl)), -sparse(diagm(lbl)))
  b = vcat(0.0, 0.0, -s.ν, 1/n*ones(Float64,n), zeros(Float64,n))
  m = quadprog(c, K, A, '<', b, -Inf, Inf, s.solver)
  # We now compute b
  α = lbl .* m.sol # For "compatibility" with the rest of the world
  # We now compute b
  s.b = intercept ? mean(lbl - K*(α .* lbl)) : 0
  # Let's "trim" our support vectors
  idx = find(x -> x > 1e-6, α)
  s.α = α[idx]
  s.sv = examples[:,idx]
  s.sl = lbl[idx]
  return s
end

function predict(s::NuVanilla, x::Dataset; raw = false)
  examples = data(x)
  K = gram(s.kernel, examples, s.sv)
  if raw
    lbl = (K * (s.α .* s.sl) + s.b)
  else
    lbl = sign(K * (s.α .* s.sl) + s.b)
    # We will break ties in the inlikely event they happen
    idx = find(x -> x == 0, lbl)
    lbl[idx] = 1
    map!(x -> s.classes[x], lbl)
  end
  return lbl
end

"""
Solves the dual of the SVM problem using dual coordinates ascent (SMO algorithm).
"""
type SMO <: KSVM
  kernel::Kernel # Chosen kernel
  C::Float64 # Penalization
  max_iter::Int
  cache::Int # allowed Kernel cache in Mb
  classes::Dict{Int,Int} # The two class labels
  sv::Matrix{Float64} # Support vectors
  y::Vector{Int} # Support labels
  α::Vector{Float64} # Coefficients of the support vectors
  b::Float64 # Intercept
  E::Vector{Float64} # Errors
  iters::Int
  SMO(g::Kernel, C::Float64) = new(g, C, 5000, 1000, Dict{Int,Int}(), Matrix{Float64}(), Vector{Int}(), Vector{Float64}(), 0.0, Vector{Float64}(), 0)
  SMO(g::Kernel, C::Float64, max_iter::Int) = new(g, C, max_iter, 1000, Dict{Int,Int}(), Matrix{Float64}(), Vector{Int}(), Vector{Float64}(), 0.0, Vector{Float64}(), 0)
  SMO(g::Kernel, C::Float64, max_iter::Int, cache::Int) = new(g, C, max_iter, cache, Dict{Int,Int}(), Matrix{Float64}(), Vector{Int}(), Vector{Float64}(), 0.0, Vector{Float64}(), 0)
end

function __findPair(j::Int, s::SMO)
  E₂ = s.E[j]
  argm = 1
  m = abs(s.E[1]-E₂)
  for i in 1:length(s.E)
    if !(0 < s.α[i] < s.C)
      continue
    elseif abs(s.E[i] - E₂) > m
      m = abs(s.E[i] - E₂)
      argm = i
    end
  end
  return argm
end

function __updateE(K::Matrix{Float64}, s::SMO)
  return K*(s.α .* s.y) - s.b - s.y
end

function __updateEj(j::Int, K::Matrix{Float64}, s::SMO)
  return dot(s.α .* s.y, view(K,:,j)) - s.b - s.y[j]
end

function __examineExample(j::Int, K::Matrix{Float64}, s::SMO)
  eps = 1e-5
  y₂ = s.y[j]
  α₂ = s.α[j]
  C = s.C
  if !(.0 < α₂ < C)
    s.E[j] = __updateEj(j, K, s)
  end
  E₂ = s.E[j]
  r₂ = E₂*y₂
  if (r₂ < -eps && α₂ < C) || (r₂ > eps && α₂ > 0)
    idx = find(x -> .0 < x < C, s.α)
    if ( length(idx) > 1)
      i = __findPair(j, s) # Heuristic 2.2
      if __optimizePair(i, j, K, s)
        return true
      end
    end
    ix = shuffle(idx)
    for i in idx
      if __optimizePair(i, j, K, s)
        return true
      end
    end
    n = length(s.α)
    idx = shuffle(1:n)
    for i in idx
      if __optimizePair(i, j, K, s)
        return true
      end
    end
  end
  return false
end

function __optimizePair(i::Int, j::Int, K::Matrix{Float64}, s::SMO)
  eps = 1e-5
  n = length(s.α)
  if i == j
    return false
  end
  b = s.b
  C = s.C
  α₁ = s.α[i]
  α₂ = s.α[j]
  y₁ = s.y[i]
  y₂ = s.y[j]
  s₁₂ = y₁*y₂
  if (.0 < α₁ < C)
    E₁ = s.E[i]
  else
    E₁ = __updateEj(i, K, s)
  end
  if (.0 < α₂ < C)
    E₂ = s.E[j]
  else
    E₂ = __updateEj(j, K, s)
  end
  if y₁ != y₂
    L = max(.0, α₂-α₁)
    H = min(C, C+α₂-α₁)
  else
    L = max(.0, α₂+α₁-C)
    H = min(C, α₂+α₁)
  end
  if L == H
    return false
  end
  K₁₁ = K[i,i]
  K₂₂ = K[j,j]
  K₁₂ = K[i,j]
  η = 2.0*K₁₂ - K₂₂ - K₁₁
  if η < .0
    # Update α₂
    α₂ += y₂*(E₂ - E₁)/η
    α₂ = clamp(α₂, L, H)
  else
    # See (19)
    # TODO: Verifier
    f₁ = y₁*(E₁ + b) - α₁*K₁₁ - s₁₂*α₂*K₁₂
    f₂ = y₂*(E₂ + b) - s₁₂*α₁*K₁₂ - α₂*K₂₂
    L₁ = α₁ + s₁₂*(α₂ - L)
    H₁ = α₁ + s₁₂*(α₂ - H)
    Lobj = L₁*f₁ + L*f₂ + 0.5*L₁^2*K₁₁+0.5*L^2*K₂₂+s₁₂*L₁*L*K₁₂
    Hobj = H₁*f₁ + H*f₂ + 0.5*H₁^2*K₁₁+0.5*H^2*K₂₂+s₁₂*H₁*H*K₁₂
    if Lobj > Hobj + eps
      α₂ = L
    elseif Lobj < Hobj - eps
      α₂ = H
    end
  end
  if α₂ < 1e-8
    α₂ = .0
  elseif α₂ > C - 1e-8
    α₂ = C
  end
  if (abs(α₂ - s.α[2]) < eps * (α₂ + s.α[2] + eps))
    return false
  end
  # Update α₁
  α₁ -= s₁₂*(α₂ - s.α[j])
  if α₁ < .0 # Should never happen. Just in case.
    α₂ += s₁₂*α₁
    α₁ = .0
  elseif α₁ > C
    α₂ += s₁₂*(α₁ - C)
    α₁ = C
  end
  # Update b
  b₁ = b + E₁ + y₁*(α₁ - s.α[i])*K₁₁ + y₂*(α₂ - s.α[j])*K₁₂
  b₂ = b + E₂ + y₁*(α₁ - s.α[i])*K₁₂ + y₂*(α₂ - s.α[j])*K₂₂
  if .0 < α₁ < C
    b = b₁
  elseif .0 < α₂ < C
    b = b₂
  else
    b = (b₁ + b₂) * 0.5
  end
  # Unbound examples have 0 error
  for k in 1:n
    if .0 < s.α[k] < C
      s.E[k] += y₁*(α₁ - s.α[i])*K[i,k] + y₂*(α₂ - s.α[j])*K[j,k] + s.b - b
    end
  end
  s.E[i] = .0
  s.E[j] = .0
  s.α[i] = α₁
  s.α[j] = α₂
  s.b = b
  return true
end

function fit(s::SMO, x::Dataset; verbose = false, threading = false, progress = false, simple = false, precomputedK = Matrix{Float64}())
  n = length(x)
  if s.cache > n*n*64/(8e+6)
    examples = collect(x)
    # Extract labels
    lbl = Vector{Int}(map(x -> x[2], examples))
    # Get the classes
    classes = sort(unique(lbl), rev = true)
    if length(classes) != 2
      error("ERROR: Not a binary classification problem")
    end
    # Remap classes to 1 / -1 and remember the mapping
    s.classes[1] = classes[1]
    s.classes[-1] = classes[2]
    pos = find(x -> x == classes[1], lbl)
    neg = find(x -> x == classes[2], lbl)
    lbl[pos] = 1
    lbl[neg] = -1
    # Extract examples
    map!(x -> x[1], examples)
    examples = hcat(examples...)
    if isempty(precomputedK)
      K = gram(s.kernel, examples)
    else
      K = precomputedK
    end
    s.y = lbl
    s.α = zeros(Float64, n)
    s.b = 0.0
    s.E = -lbl

    numChanged = 0
    examineAll = true

    while ((numChanged > 0 || examineAll) && s.iters < s.max_iter)
      numChanged = 0
      if verbose
        info("examineAll at iteration $(s.iters): $examineAll \n")
      end
      if examineAll
        for j in 1:n
          numChanged += __examineExample(j, K, s) ? 1 : 0
        end
      else
        for j in 1:n
          if s.α[j] != .0 && s.α[j] != s.C
            numChanged += __examineExample(j, K, s) ? 1 : 0
          end
        end
      end
      if examineAll
        examineAll = false
      elseif numChanged == 0
        examineAll = true
      end
      s.iters += 1
      if verbose
        info("numChanged at iteration $(s.iters): $numChanged \n")
        err = mean(sign(K * (s.α .* s.y) - s.b) .== s.y)
        info("Error: $err \n")
      end
    end
  else
    println("Caching Not Implemented For the moment, just use a larger cache.")
  end
  idx = find(x -> x > .0, s.α)
  s.α = s.α[idx]
  s.sv = examples[:,idx]
  s.y = s.y[idx]
  return s
end

function predict(s::SMO, x::Dataset; raw = false)
  examples = data(x)
  K = gram(s.kernel, examples, s.sv)
  if !raw
    y = sign(K * (s.α .* s.y) - s.b) # Be careful Pratt uses -b
    # We will break ties in the inlikely event they happen
    idx = find(x -> x == .0, y)
    y[idx] = 1
    map!(x -> s.classes[x], y)
  else
    y = K * (s.α .* s.y) - s.b
  end
  return y
end

"""
A particular instance of SVM using SGD as it's solver.
"""
type Pegasos <: KSVM
  kernel::Kernel # Chosen kernel
  C::Float64 # Penalization
  epochs::Int # Number of epochs
  nbatch::Int # Batch size
  classes::Dict{Int,Int} # The two class labels
  sv::Matrix{Float64} # Support vectors
  sl::Vector{Float64} # Support labels
  α::Vector{Float64} # Coefficients of the support vectors
  b::Float64 # Intercept
  Pegasos(g::Kernel, C::Float64, e::Int, nb::Int) = new(g, C, e, nb, Dict{Int,Int}(), Matrix{Float64}(), Vector{Float64}(), 0.0)
end

"""
A particular instance of SVM using SGD as it's solver while keeping a budget of support vectors. See [1].

[1] Z. Wang, K. Crammer, and S. Vucetic, “Breaking The Curse of Kernelization: Budgeted Stochastic Gradient Descent for Large-scale Svm Training,” J. Mach. Learn. Res., vol. 13, pp. 3103–3131, 2012.
"""
type BSGD <: KSVM
  kernel::Kernel # Chosen kernel
  C::Float64 # Penalization
  B::Int # Budget
  epochs::Int # Number of epochs
  nbatch::Int # Batch size
  classes::Dict{Int,Int} # The two class labels
  sv::Matrix{Float64} # Support vectors
  α::Vector{Float64} # Coefficients of the support vectors
  BSGD(g::Kernel, C::Float64, B::Int, e::Int, nb::Int) = new(g, C, B, e, nb, Dict{Int,Int}(), Matrix{Float64}(), Vector{Float64}())
  BSGD(g::Kernel, C::Float64, B::Int) = new(g, C, B, 1, 0, Dict{Int,Int}(), Matrix{Float64}(), Vector{Float64}())
  BSGD(g::Kernel, C::Float64, B::Int, e::Int) = new(g, C, B, e, 0, Dict{Int,Int}(), Matrix{Float64}(), Vector{Float64}())
end

function __objective(m, k12 ,x)
  return -(m*k12^((1.0-x)^2) + (1-m)*k12^(x^2))
end

function minimizeLoss(Kp, α, nsvs, p)
  losses = Vector{Float64}(nsvs)
  candidates = Vector{Float64}(nsvs)
  for q = 1:nsvs
    k12 = Kp[q]
    m = α[p]/(α[p] + α[q])

    if (m*(1-m) > 0)
      res = optimize(x -> __objective(m, k12, x), .0, 1.0)
    elseif m > 0
      res = optimize(x -> __objective(m, k12, x), 1.0, 6.0)
    else
      res = optimize(x -> __objective(m, k12, x), -5.0, 0.0)
    end

    h = Optim.minimizer(res)
    candidates[q] = h
    loss = Optim.minimum(res)
    losses[q] = loss
  end
  q = indmin(losses)
  z = candidates[q]
  k12 = Kp[q]
  k1_z = k12^((1.0-z)^2)
  k2_z = k12^(z^2)
  α_z = α[p].*k1_z + α[q].*k2_z # Eq 19
  return z, α_z, q
end

function fit(svm::BSGD, X::Dataset; threading = false, progress = false, precomputedK = Matrix{Float64}())
  if !(typeof(svm.kernel) <: GaussianKernel)
    error("For the moment BGSD needs a GaussianKernel to be able to use merging.")
  end
  # Get the classes
  classes = sort(unique(labels(X)), rev = true)
  if length(classes) != 2
    error("ERROR: Not a binary classification problem")
  end
  # Remap classes to 1 / -1 and remember the mapping
  svm.classes[1] = classes[1]
  svm.classes[-1] = classes[2]
  label = Dict{Int,Int}()
  label[classes[1]] = 1
  label[classes[2]] = -1
  # BSGD Proper
  n = length(X)
  d = size(X[1,1])[1]
  α = zeros(svm.B+1)
  svs = zeros(Float64, (d,svm.B+1)) # The selected SVs, we will only store them at the very end, we also have one extra SV which is the one store before budgetting
  last_free = 1 # Where to start writing
  for epoch = 1:svm.epochs
    index = shuffle(1:n)
    for i = 1:n
      j = index[i]
      sample = X[j]
      x, y = sample[1], sample[2]
      y = label[y]
      t = (epoch-1)*n + i

      if t == 1 # initialization
        svs[:,1] = x
        α[1] = y
        last_free = 2
      else
        η = svm.C / t

        K = gram(svm.kernel, svs, x)
        ypred = dot(K, α)  # get the prediction vector, each element is the prediction of the class-specific weight
        α = (1 - η / svm.C) .* α

        if ypred <= 1
          svs[:,last_free] = x
          α[last_free] = η*y
          last_free = last_free + 1
          # Budget Maintenance
          if last_free > svm.B+1 # No more free space
            p = findmin(α .^ 2)[2] # find the SV with the smallest norm value of alpha
            xp = svs[:,p]
            Kp = gram(svm.kernel, svs, xp) # calulate the kernel distance between x_p (the one with the smallest norm value of alpha) and the remaining SVs for the following function

            # solve the optimization between Eq.(21) and (22)
            z, α_z, q = minimizeLoss(Kp, α, svm.B+1, p)

            xq = svs[:,q]

            # remove two old SVs and add the new SV
            M = max(p,q)
            m = max(p,q)
            if M < svm.B
              svs[:,p] = svs[:,svm.B]
              α[p] = α[svm.B]
              svs[:,q] = svs[:,svm.B+1]
              α[q] = α[svm.B+1]
            elseif m < svm.B
              notM = p == M ? q : p
              svs[:,m] = svs[:,notM]
              α[m] = α[notM]
            end
            svs[:,svm.B] = z
            α[svm.B] = α_z
            last_free = svm.B + 1
          end
        end
      end
    end
  end
  svm.sv = svs[:,1:svm.B]
  svm.α = α[1:svm.B]
  return svm
end

function predict(s::BSGD, x::Dataset; raw = false)
  examples = data(x)
  K = gram(s.kernel, examples, s.sv)
  if !raw
    lbl = sign(K * s.α)
    # We will break ties in the inlikely event they happen
    idx = find(x -> x == 0, lbl)
    lbl[idx] = 1
    map!(x -> s.classes[x], lbl)
  else
    lbl = K * s.α
  end
  return lbl
end

doc"""
Build a multiclass SVM from ``\frac{C (C-1)}{2}`` binary SVMs in a OneVsOne fashion.
"""
type OneVsOne <: KSVM
  prototype::KSVM
  svms::Vector{KSVM}
  OneVsOne(p) = new(p, Vector{KSVM}())
end

function fit(s::OneVsOne, x::Dataset; threading = false, progress = true, precompute = false)
  classes = unique(labels(x))
  C = length(classes)
  temp = collect(product(repeated(classes,2)...))
  pairs = Vector{Tuple{Int,Int}}(Int(C*(C-1) // 2))
  i = 1
  for p in temp
    if p[1] < p[2]
      pairs[i] = p
      i += 1
    end
  end
  # Now for each pair we will build the corresponding classifier
  ns = length(pairs)
  s.svms = Vector{KSVM}(ns) # Initializing vector
  progress = threading ? false : progress
  if progress
    progressBar = Progress(ns, 1, "OneVsOne:", 50)
  else
    warn("The progress bar has been disabled (either manually or because Threading has been activated). Please be patient.")
  end
  # Convex.jl uses global variables and therefore CANNOT be used multithreaded, this is unfortunate so we disable threading if we are in the Vanilla or NuVanilla case.
  THREADING_DISALLOWED = (typeof(s.prototype) <: Vanilla || typeof(s.prototype) <: NuVanilla)
  if THREADING_DISALLOWED || !threading
    if threading
      println("Threading disabled because of incompatibilities with the solver.")
    end
    for i = 1:ns
      # First let's build a Dataset with only the corresponding example
      l = labels(x)
      pair = pairs[i]
      pos = find(x -> x == pair[1], l)
      neg = find(x -> x == pair[2], l)
      newLabels = vcat(l[pos], l[neg])
      posSamples = data(x)[:,pos]
      negSamples = data(x)[:,neg]
      samples = hcat(posSamples,negSamples)
      dataset = typeof(x)(samples, newLabels)
      # Now we build our classifier
      clf = deepcopy(s.prototype)
      fit(clf, dataset)
      # We store it
      s.svms[i] = clf
      if progress
        next!(progressBar)
      end
    end
  else
    Threads.@threads for i = 1:ns
      # First let's build a Dataset with only the corresponding example
      l = labels(x)
      pair = pairs[i]
      pos = find(x -> x == pair[1], l)
      neg = find(x -> x == pair[2], l)
      newLabels = vcat(l[pos], l[neg])
      posSamples = data(x)[:,pos]
      negSamples = data(x)[:,neg]
      samples = hcat(posSamples,negSamples)
      dataset = typeof(x)(samples, newLabels)
      # Now we build our classifier
      clf = deepcopy(s.prototype)
      fit(clf, dataset)
      # We store it
      s.svms[i] = clf
      if progress
        next!(progressBar)
      else
        println("Classifier $i: ✓")
      end
    end
  end
  return s
end

"""
A helper function to tally predictions i.e. take a majority vote of classifiers.
"""
function vote(votes)
  t = Dict{Int,Int}()
  for v in votes
    if !haskey(t, v)
      t[v] = 0
    end
    t[v] += 1
  end
  return reduce((x, y) -> t[x] >= t[y] ? x : y, keys(t))
end

function prob(votes)
  t = Dict{Int,Float64}()
  for v in votes
    if !haskey(t, v)
      t[v] = 0.
    end
    t[v] += 1.
  end
  n = length(votes)
  for v in votes
    t[v] /= n
  end
  return sort(collect(t))
end

function predict(s::OneVsOne, x::Dataset; raw = false)
  n = length(x)
  preds = Vector{Vector{Int}}()
  # Let's initialize our predictions (with nothing)
  for i = 1:n
    push!(preds, Vector{Int}())
  end
  # for each classifier we will predict the class
  for svm in s.svms
    pred = predict(svm, x)
    # We add this classifier predictions to all the predictions
    for i = 1:n
      push!(preds[i], pred[i])
    end
  end
  # We now tally the votes
  votes = map(vote, preds)
  return votes
end

doc"""
Build a multiclass SVM from ``C`` binary SVMs in a OneVsRest fashion.
"""
type OneVsRest <: KSVM
  prototype::KSVM
  svms::Vector{KSVM}
  classes::Vector{Int}
  OneVsRest(p) = new(p, Vector{KSVM}(), Vector{Int}())
end

function fit(s::OneVsRest, x::Dataset; threading = false, progress = true, precompute = false)
  # TODO: In one vs rest we can build a SINGLE matrix K and share it between all subproblems
  classes = unique(labels(x))
  s.classes = classes
  C = length(classes)
  s.svms = Vector{KSVM}(C) # Initializing vector
  if precompute
    examples = collect(x)
    map!(x -> x[1], examples)
    examples = hcat(examples...)
    K = gram(s.prototype.kernel, examples)
  end
  progress = threading ? false : progress
  if progress
    progressBar = Progress(C, 1, "OneVsAll:", 50)
  else
    warn("The progress bar has been disabled (either manually or because Threading has been activated). Please be patient.")
  end
  l = labels(x)
  Threads.@threads for i = 1:C
    # First let's build a Dataset with only the corresponding example
    c = classes[i]
    pos = find(x -> x == c, l)
    neg = find(x -> x != c, l)
    newLabels = Vector{Int}(length(l))
    newLabels[pos] = c
    newLabels[neg] = -(c+1)
    samples = data(x)
    dataset = typeof(x)(samples, newLabels)
    # Now we build our classifier
    clf = deepcopy(s.prototype)
    if precompute
      SVM.fit(clf, dataset; precomputedK = K)
    else
      SVM.fit(clf, dataset)
    end
    # We store it
    s.svms[i] = clf
    if progress
      next!(progressBar)
    else
      println("Classifier $i: ✓")
    end
  end
end

@inline function confidence(preds)
  return reduce((x, y) -> preds[x] >= preds[y] ? x : y, keys(preds))
end

function predict(s::OneVsRest, x::Dataset)
  # TODO: Train a logistic classifier during training to learn how to balance the different predictions of different kernels
  n = length(x)
  C = length(s.classes)
  preds = Vector{Dict{Int, Float64}}(n)
  # Initialize Dicts
  for i in 1:n
    preds[i] = Dict{Int, Float64}()
  end
  # for each classifier we will compute the confidence for class i
  for svm in s.svms
    pred = predict(svm, x; raw = true)
    class = svm.classes[1]
    for i in 1:n
      preds[i][class] = pred[i]
    end
  end
  # We now tally the votes
  votes = map(confidence, preds)
  return votes
end

doc"""
Create an ensemble of SVMs by bagging.
"""
type Bagging <: KSVM
  prototype::KSVM
  fraction::Float64
  bags::Int
  svms::Vector{KSVM}
  Bagging(p) = new(p, 0.1, 10, Vector{KSVM}())
  Bagging(p, f) = new(p, f, 10, Vector{KSVM}())
  Bagging(p, f, b) = new(p, f, b, Vector{KSVM}())
end

function fit(b::Bagging, x::Dataset; threading = false)
  b.svms = Vector{typeof(b.prototype)}(b.bags)
  for i in 1:b.bags
    n = data(x)
    subset = take(shuffle(1:n), Int(floor(n*b.fraction)))
    xb = typeof(x)(data(x)[subset,:], labels(x)[subset,:])
    clf = deepcopy(b.prototype)
    fit(clf, xb)
    b.svms[i] = clf
  end
end

function predict(b::Bagging, x::Dataset)
  n = length(x)
  preds = Vector{Vector{Int}}()
  # Let's initialize our predictions (with nothing)
  for i = 1:n
    push!(preds, Vector{Int}())
  end
  # for each classifier we will predict the class
  for svm in b.svms
    pred = predict(svm, x; raw = true)
    # We add this classifier predictions to all the predictions
    for i = 1:n
      push!(preds[i], pred[i])
    end
  end
  # We now tally the votes
  return map(vote, preds)
end

end
