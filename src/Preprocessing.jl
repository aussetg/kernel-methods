module Preprocessing

export centering, centering!, whiten, whiten!, whitent, whitent!

function centering(X)
  m = mapslices(mean,X,2)
  return X .- m , m
end

function centering!(X)
  m = mapslices(mean,X,2)
  return X .- m
end

function whiten(X)
  (n,m) = size(X)
  C = 1/n * (X' * X)
  U, S, V = svd(C)
  return (U * diagm(S.^(-0.5)) * U' * X')', U, D
end

function whiten!(X)
  (n,m) = size(X)
  C = 1/n * (X' * X)
  U, S, V = svd(C)
  return (U * diagm(S.^(-0.5)) * U' * X')'
end

function whitent(X)
  (m,n) = size(X)
  C = 1/n * (X * X')
  U, S, V = svd(C)
  return (U * diagm(S.^(-0.5)) * U' * X), U, diagm(D)
end

function whitent!(X)
  (m,n) = size(X)
  C = 1/n * (X * X')
  U, S, V = svd(C)
  return (U * diagm(S.^(-0.5)) * U' * X)
end

end
