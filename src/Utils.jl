module Utils

export writePredictions, getData, getLabels

function writePredictions(preds::Vector{Int}, filename::String)
  n = length(preds)
  p = Array{Any,2}((n+1,2))
  p[1,:] = ["Id" "Prediction"]
  for i = 1:n
    p[1+i, 1] = i
    p[1+i, 2] = preds[i]
  end
  writecsv(filename, p)
end

function getData(filename::String)
  d = readcsv(filename)
  d = d[:,1:(end-1)] # There is a stray ; at the end of each column
  d = Array{Float64,2}(d)
  return d
end

function getLabels(filename::String)
  d = readcsv(filename)
  d = d[2:end,2]
  d = Vector{Int}(d)
  return d
end

end
