push!(LOAD_PATH,"src/")
using Distributions
using Data, Kernels, SVM, CrossVal, Utils, PCA
#using Plots
#gr()

# Binary

c1 = rand(MvNormal([0,0], 0.3*eye(2)),500)
c2 = rand(MvNormal([3,0], 0.3*eye(2)),500)
examples = hcat(c1, c2)
lbl = vcat(ones(Int,500), 2*ones(Int,500))

x = VanillaDataset(examples, lbl)
g = GaussianKernel()
s = SVM.SMO(g, 1.0, 1000)

SVM.fit(s, x)
@time CV(s, x)

preds = predict(s, x)
mean(preds .== lbl)

## BSGD

s_b = SVM.BSGD(g, 1.0, 100)

@time SVM.fit(s_b, x)
@step SVM.fit(s_b, x)

# Multi-class

c1 = rand(MvNormal([0,0], 0.3*eye(2)),5000)
c2 = rand(MvNormal([3,0], 0.3*eye(2)),5000)
c3 = rand(MvNormal([0,3], 0.3*eye(2)),5000)
c4 = rand(MvNormal([3,3], 0.3*eye(2)),5000)

examples = hcat(c1, c2, c3, c4)
lbl = vcat(ones(Int,5000), 2*ones(Int,5000), 3*ones(Int,5000), 4*ones(Int,5000))

scatter(c1[:,1], c1[:,2], leg=false)
scatter!(c2[:,1], c2[:,2], leg=false)
scatter!(c3[:,1], c3[:,2], leg=false)
scatter!(c4[:,1], c4[:,2], leg=false)

type container
  v::Vector{Float64}
end

function fillContainer(c::container)
  for i in 1:length(c.v)
    c.v[i] = 1
  end
end

function fillVector(v::Vector{Float64})
  for i in 1:length(v)
    v[i] = 1
  end
end

v = Vector{Float64}(100000)
c = container(v)

@time fillContainer(c)
@time fillVector(v)

x = VanillaDataset(examples, lbl)

g = GaussianKernel()
svm_base = SVM.SMO(g, 1.0, 100, 10000)
s = OneVsRest(svm_base)

@time SVM.fit(s, x)
@time CV(s, x; threading = false)

preds = SVM.predict(s, x)
mean(preds .== lbl)

## Real data
using Data, Kernels, Utils

X = getData("data/Xtr.csv")

g = GaussianKernel()

K = @time gram(g, Xt, Xt)

# PCA
PCA.fit(p, X, 2)

c1 = rand(MvNormal([0,0], 0.3*eye(2)),500)
c2 = rand(MvNormal([3,0], 0.3*eye(2)),500)
c3 = c1 + c2

X = vcat(c1, c2, c3)

p = pca()
PCA.fit(p, X, 2)
