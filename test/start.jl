push!(LOAD_PATH,"src/")
using Data, Kernels, SVM, CrossVal, Utils, ImageUtils, PCA, Preprocessing
#using Plots

Xtr = getData("data/Xtr.csv")
Ytr = getLabels("data/Ytr.csv")

Xtr = Xtr' # Julia prefers column major ordering. All functions will use it

# First let's build our augmented dataset (just mirroring right now) of HoG features

## We will first convert image to grayscale

function toImage(v::Vector{Float64}, h::Int, w::Int)
  temp = reshape(v, (w, h, 3))
  im = zeros(Float64, (h, w, 3))
  for i = 1:3
    im[:,:,i] = rotl90(temp[:,:,i])
  end
  return im
end

#function grayscale(image::Array{Float64,3})
#  im = 0.2989.*image[:,:,1] + 0.5870.*image[:,:,2] + 0.1140.*image[:,:,3]
#end

function grayscale(image::Array{Float64,3})
  im = 0.333.*image[:,:,1] + 0.333.*image[:,:,2] + 0.333.*image[:,:,3]
end

dog = grayscale(toImage(Xtr[:,16], 32, 32))

#heatmap(dog)

function mirror(image::Matrix{Float64})
  return image[:,end:-1:1]
end

function mirror(image::Array{Float64,3})
  return image[:,end:-1:1,:]
end

#heatmap(mirror(dog))
#heatmap(mirror(toImage(Xtr[:,16], 32, 32))[:,:,3])
#heatmap(toImage(Xtr[:,16], 32, 32)[:,:,3])

## Let's now build a dataset of size 324*10000

augData = Matrix{Float64}((324,10000))
lbl = Vector{Int}(10000)

Threads.@threads for i =1:10000
  im = grayscale(toImage(Xtr[:,i], 32, 32))
  augData[:,i] = hog(im, (8,8), (2,2))
  lbl[i] = Ytr[i]
  im = mirror(im)
  augData[:,5000+i] = hog(im, (8,8), (2,2))
  lbl[5000+i] = Ytr[i]
end

#augData = centeringt!(augData)
#augData = whitent!(augData)


## We will reduce the dimension to 100

#p = pca()
#PCA.fit(p, augData, 100)

#augData = transform(p, augData)

## And now the dataset

x = VanillaDataset(augData, lbl)

# Let's build our classifier

g = GaussianKernel(0.5)
s = SMO(g, 1.7, 5000)
s = Vanilla(g, 1.7, :hinge, MosekSolver())

clf = OneVsRest(s)
SVM.fit(clf, x; progress = true, threading = true)
# Ipopt:
# Gurobi: NA / | MB /
# Mosek: 10:46 / 58:29 | MB /

clf = OneVsOne(s)
@time SVM.fit(clf, x; progress = true, threading = true)
# Ipopt: PC 3:23 / | MB /
# Gurobi: NA / | MB /
# Mosek: 0:52 / 4:10 | MB /
# SMO:

plot(sort(clf.svms[5].α))

# As a sanity check let's check our training accuracy

preds = predict(clf, x)
mean(preds .== lbl)

CV(clf, x; threading = false)

function gridsearchOvA(i)
  g = GaussianKernel(i)
  s = SMO(g, 5.0, 2000)
  clf = OneVsRest(s)
  return CV(clf, x; threading = false)
end

function gridsearchOvO(i)
  g = GaussianKernel(i)
  s = SMO(g, 5.0, 2000)
  clf = OneVsOne(s)
  return CV(clf, x; threading = false)
end

map(gridsearchOvA, [0.3, 0.4, 0.5, 0.6])
map(gridsearchOvO, [0.5, 1.0, 2.0, 3.0, 4.0, 5.0])

function gridsearchOvOb(i)
  g = GaussianKernel(1.0)
  s = SMO(g, i, 500)
  clf = OneVsOne(s)
  return CV(clf, x; threading = false)
end

function gridsearchOvAb(i)
  g = GaussianKernel(0.6)
  s = SMO(g, i, 2000)
  clf = OneVsRest(s)
  return CV(clf, x; threading = false)
end

map(gridsearchOvAb, [4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0])
map(gridsearchOvOb, [11.0, 12.0, 13.0, 14.0])

# OvA?
clfb = OneVsRest(s)
SVM.fit(clfb, x; progress = true, threading = false)

preds = predict(clfb, x)
mean(preds .== lbl)

# We now import our test set

Xte = getData("data/Xte.csv")
Xte = Xte'

p, nt = size(Xte)

augDataTest = Matrix{Float64}((324,nt))

Threads.@threads for i = 1:nt
  im = grayscale(toImage(Xte[:,i], 32, 32))
  augDataTest[:,i] = hog(im)
end

#augDataTest = transform(p, augDataTest)

xt = VanillaDataset(augDataTest)

preds = predict(clf, xt)

# We write our predictions

writePredictions(preds, "data/Yte.csv")
