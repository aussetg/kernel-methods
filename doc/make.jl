push!(LOAD_PATH,"src/")
using Documenter, Data, Kernels, SVM, KNN, Approximations, CrossVal, Utils, ImageUtils, Preprocessing, PCA

makedocs(
        format = :html,
        sitename = "kernel-methods",
        authors = "Guillaume Ausset",
        repo = "https://gitlab.com/aussetg/kernel-methods/blob/{commit}{path}#L{line}"
        )
