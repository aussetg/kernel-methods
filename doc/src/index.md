# kernel-methods Documentation

```@contents
```

This package is provided as a solution for the [data challenge](https://inclass.kaggle.com/c/kernel-methods-for-machine-learning-data-challenge) for the course ["Kernel Methods for Machine Learning"](http://members.cbio.mines-paristech.fr/~jvert/svn/kernelcourse/course/2017mva/index.html) at the ENS Cachan for the Master M.A.S.H.

## The package `kernel-methods`

## Module `Data`

```@autodocs
Modules = [Data]
Private = false
Order = [:type, :function]
```

## Module `Kernels`

```@autodocs
Modules = [Kernels]
Private = false
Order = [:type, :function]
```

## Module `SVM`

```@autodocs
Modules = [SVM]
Private = false
Order = [:type, :function]
```

## Module `KNN`

```@autodocs
Modules = [KNN]
Private = false
Order = [:type, :function]
```

## Module `Preprocessing`

```@autodocs
Modules = [Preprocessing]
Private = false
Order = [:type, :function]
```

## Module `PCA`

```@autodocs
Modules = [PCA]
Private = false
Order = [:type, :function]
```

## Module `CrossVal`

```@autodocs
Modules = [CrossVal]
Private = false
Order = [:type, :function]
```

## Module `ImageUtils`

```@autodocs
Modules = [ImageUtils]
Private = false
Order = [:type, :function]
```

## Module `Utils`

```@autodocs
Modules = [Utils]
Private = false
Order = [:type, :function]
```

## Index

```@index
```
