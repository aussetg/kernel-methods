#!/bin/bash
DOCKERID="$(sudo docker run -v $PWD:/home/kernel -it -d aussetg/julia-mosek)"
sudo docker exec $DOCKERID bash -c 'cd /home/kernel; julia --color=yes start.jl'
