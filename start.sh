#!/bin/bash
NCORES="$(getconf _NPROCESSORS_ONLN)"
env JULIA_NUM_THREADS=$NCORES julia --color=yes start.jl
